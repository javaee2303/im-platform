package com.qf.commons.aspect;


import com.qf.commons.domain.Codes;
import com.qf.commons.exception.ServiceException;
import com.qf.commons.utils.AuthUtils;
import com.qf.commons.utils.JwtUtils;
import jakarta.servlet.http.HttpServletRequest;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 登录认证的AOP
 */
@Aspect
@Component
public class LoginAuthAspect {

    @Around("@annotation(com.qf.commons.aspect.annotation.GetUser)")
    public Object loginTokenParse(ProceedingJoinPoint joinPoint){

        //1、接收请求获取令牌（请求头）
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        HttpServletRequest request = sra.getRequest();
        String loginToken = request.getHeader("Login-Token");

        //2、解析
        //3、获取令牌中的uid
        Integer uid = JwtUtils.parseJwtToken(loginToken, "uid");

        //4、判断id是否存在
        if (uid == null) {
            //  -- 不存在   返回错误，认证失败，客户端重新登录
            throw new ServiceException(Codes.AUTH_ERROR);
        }

        //  -- 存在  uid存到ThreadLocal
        AuthUtils.set(uid);

        //5、放行 - 执行核心
        try {
            //6、返回核心业务结果
            return joinPoint.proceed();
        } catch (ServiceException e) {
            throw e;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        } finally {
            //7、清空ThreadLocal
            AuthUtils.clear();
        }
    }
}
