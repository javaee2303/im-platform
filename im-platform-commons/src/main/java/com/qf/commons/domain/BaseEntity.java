package com.qf.commons.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class BaseEntity implements Serializable {
    //创建时间
    private Date createTime = new Date();
    //最后修改时间
    private Date updateTime = new Date();
    //状态
    private Integer status = 0;
    //删除标识 0-正常 1-已经删除
    private Integer delFlag = 0;
}
