package com.qf.commons.domain;

public enum Codes {

    SUCC(200, "成功"),
    FAIL(500, "服务器异常!"),
    NOT_POWER(501, "权限不足"),
    PARAMS_BIND(502, "参数校验失败"),
    USER_EXISTS(503, "用户名已经存在"),
    EMAIL_EXISTS(504, "邮箱已经存在"),
    FILE_NOT_FOUND(505, "文件未找到"),
    LOGIN_FAIL(506, "用户名或者密码错误"),
    AUTH_ERROR(507, "令牌为空或者已过期"),
    USER_NOT_FOUND(508, "用户名不存在"),
    CODE_ERROR(509, "验证码错误"),
    REQUEST_EXISTS(510, "不能重复发送好友申请"),
    FRIENDS_EXISTS(511, "已经是好友关系"),
    NOT_REQUEST_SELF(512, "不能添加自己为好友");

    private Integer code;
    private String msg;

    Codes(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
