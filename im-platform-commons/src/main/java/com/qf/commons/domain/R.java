package com.qf.commons.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 统一返回的对象
 */
@Data
@Accessors(chain = true)
public class R<T> implements Serializable {

    private Integer code;
    private String msg;
    private T data;

    public static <S> R succ(S data){
        return new R()
                .setCode(Codes.SUCC.getCode())
                .setMsg(Codes.SUCC.getMsg())
                .setData(data);
    }

    public static R succ(){
        return succ(null);
    }

    public static <S> R fail(Codes codes, S data){
        return new R()
                .setCode(codes.getCode())
                .setMsg(codes.getMsg())
                .setData(data);
    }

    public static <S> R fail(Codes codes){
        return fail(codes, null);
    }
}
