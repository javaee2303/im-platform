package com.qf.commons.exception;

import com.qf.commons.domain.Codes;
import lombok.Data;

@Data
public class ServiceException extends RuntimeException{

    //异常的枚举对象
    private Codes codes;

    public ServiceException(Codes codes) {
        this.codes = codes;
    }
}
