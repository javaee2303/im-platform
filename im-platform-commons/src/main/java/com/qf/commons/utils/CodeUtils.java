package com.qf.commons.utils;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * 验证码管理
 */
public class CodeUtils {

    private static Map<String, Val> codesMap = new ConcurrentHashMap<>();

    public static void putCode(String key, Integer code, Integer ttl, TimeUnit timeUnit){
        Val val = new Val()
                .setData(code)
                .setTtl(System.currentTimeMillis() + timeUnit.toMillis(ttl));
        codesMap.put(key, val);
    }

    public static Integer getCode(String key){
        Val<Integer> val = codesMap.get(key);
        if (val.getTtl() < System.currentTimeMillis()) {
            remove(key);
            return null;
        }
        return val.getData();
    }

    public static void remove(String key) {
        codesMap.remove(key);
    }

    @Data
    @Accessors(chain = true)
    private static class Val<T> {
        private T data;
        private Long ttl;
    }
}
