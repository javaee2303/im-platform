package com.qf.im.controller;

import com.qf.commons.domain.Codes;
import com.qf.commons.domain.R;
import com.qf.commons.exception.ServiceException;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.UUID;

/**
 * 文件上传下载
 */
@RestController
@RequestMapping("/file")
public class FileController {

    @Value("${file.uploader}")
    private String uploadPath;

    /**
     * 文件上传
     * @return
     */
    @RequestMapping("/uploader")
    public R upload(MultipartFile file){

        File filePath = new File(uploadPath);
        if (!filePath.exists()) {
            filePath.mkdirs();
        }

        //准备文件名称
        String fileName = UUID.randomUUID().toString();

        //
        try (
                InputStream in = file.getInputStream();
                OutputStream out = new FileOutputStream(new File(filePath, fileName));
        ){
            IOUtils.copy(in, out);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return R.succ(fileName);
    }

    /**
     * 文件的下载
     * @param fileName
     * @param response
     */
    @RequestMapping("/download")
    public void download(String fileName, HttpServletResponse response){
        File file = new File(uploadPath, fileName);
        if (!file.exists())
            throw new ServiceException(Codes.FILE_NOT_FOUND);

        try(
                InputStream in = new FileInputStream(file);
                OutputStream out = response.getOutputStream();
        ) {
            IOUtils.copy(in, out);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 文件重命名的功能
     */
    public void fileReName(){

    }

    /**
     * 文件过滤
     */
    public void fileFilter(){

    }
}
