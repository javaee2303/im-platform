package com.qf.im.controller;


import com.qf.commons.domain.R;
import com.qf.im.entity.vo.FriendsVo;
import com.qf.im.service.FriendsRelationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 好友关系表(FriendsRelations)表控制层
 *
 * @author makejava
 * @since 2023-07-28 11:00:38
 */
@RestController
@RequestMapping("friendsRelations")
public class FriendsRelationsController{
    /**
     * 服务对象
     */
    @Autowired
    private FriendsRelationsService friendsRelationsService;

    /**
     * 查询当前用户的好友列表
     * @return
     */
    @RequestMapping("/friendsList")
    public R friendsList(){
        List<FriendsVo> friendsVos = friendsRelationsService.friendsList();
        return R.succ(friendsVos);
    }

}

