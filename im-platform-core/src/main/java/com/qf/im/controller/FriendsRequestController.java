package com.qf.im.controller;


import com.qf.commons.domain.R;
import com.qf.im.entity.FriendsRequest;
import com.qf.im.entity.vo.FriendsRequestVo;
import com.qf.im.entity.vo.RequestHandlerVo;
import com.qf.im.service.FriendsRequestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 好友申请表(FriendsRequest)表控制层
 *
 * @author makejava
 * @since 2023-07-28 11:00:39
 */
@RestController
@RequestMapping("friendsRequest")
@Slf4j
public class FriendsRequestController {
    /**
     * 服务对象
     */
    @Autowired
    private FriendsRequestService friendsRequestService;

    /**
     * 添加好友申请信息
     * @return
     */
    @RequestMapping("/insert")
    public R insert(FriendsRequest friendsRequest){
        log.debug("[friends request] 发送好友申请信息 - {}", friendsRequest);
        friendsRequestService.save(friendsRequest);
        return R.succ();
    }

    /**
     * 查询发送给我的好友申请的待处理数量
     * @return
     */
    @RequestMapping("/requestCount")
    public R requestCount(){
        int count = friendsRequestService.queryRequestCountByMe();
        return R.succ(count);
    }

    /**
     * 查询发送给我的好友申请列表
     * @return
     */
    @RequestMapping("/queryListByMe")
    public R queryListByMe(){
        List<FriendsRequestVo> friendsRequestVos = friendsRequestService.queryRequestListByMe();
        return R.succ(friendsRequestVos);
    }

    /**
     * 好友申请的处理
     * @return
     */
    @RequestMapping("/requestHandler")
    public R requestHandler(@Validated RequestHandlerVo requestHandlerVo){
        log.debug("[request handler] 处理好友申请记录 - {}", requestHandlerVo);
        friendsRequestService.requestHandler(requestHandlerVo);
        return R.succ();
    }
}

