package com.qf.im.controller;

import com.qf.commons.aspect.annotation.GetUser;
import com.qf.commons.utils.AuthUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {

    @RequestMapping("/query")
    @GetUser
    public String test(){
        Integer uid = AuthUtils.get();
        log.debug("测试的controller方法 - {}", uid);
        return "succ";
    }
}
