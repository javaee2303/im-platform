package com.qf.im.controller;


import com.qf.commons.domain.Codes;
import com.qf.commons.domain.R;
import com.qf.commons.exception.ServiceException;
import com.qf.commons.utils.CodeUtils;
import com.qf.commons.utils.JwtUtils;
import com.qf.im.entity.User;
import com.qf.im.service.UserService;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 用户表(User)表控制层
 *
 * @author makejava
 * @since 2023-07-26 10:14:24
 */
@RestController
@RequestMapping("/user")
@Slf4j
@Validated
public class UserController {
    /**
     * 服务对象
     */
    @Autowired
    private UserService userService;


    /**
     * 用户注册
     * @return
     */
    @RequestMapping("/register")
    public R register(@Validated User user){
        log.debug("[user register] 用户注册 - {}", user);
        userService.save(user);
        return R.succ("恭喜你注册成功！");
    }

    /**
     * 登录
     * @return
     */
    @RequestMapping("/login")
    public R login(@NotBlank(message = "用户名不能为空") String username, @NotBlank(message = "密码不能为空") String password){
        log.debug("[user login] 用户登录 - {} - {}", username, password);
        User user = userService.queryByUserName(username);

        if (user == null || !user.getPassword().equals(password)) {
            throw new ServiceException(Codes.LOGIN_FAIL);
        }

        //将用户信息转换成jwt令牌
        String token = JwtUtils.createJwtToken()
                .add("uid", user.getId())
                .add("header", user.getHeader())
                .add("nickname", user.getNickname())
                .build();

        //登录成功
        return R.succ(token);
    }

    /**
     * 通过邮箱发送验证码
     * @return
     */
    @RequestMapping("/sendCode")
    public R sendCode(@NotBlank(message = "用户名不能为空") String username){
        log.debug("[send code] 发送验证码找回密码 - {}", username);
        //发送邮件
        userService.sendEmailCode(username);
        //4、返回成功
        return R.succ();
    }

    @RequestMapping("/updatePassword")
    public R updatePassword(@NotBlank(message = "用户名不能为空") String username,
                            @NotBlank(message = "新密码不能为空") String password,
                            @NotNull(message = "验证码不能为空") Integer code) {
        //校验验证码
        Integer sendCode = CodeUtils.getCode(username);
        if (sendCode == null || !sendCode.equals(code)) {
            //验证码校验不通过
            throw new ServiceException(Codes.CODE_ERROR);
        }
        //移除验证
        CodeUtils.remove(username);
        //修改密码
        //update xxxx set password = ? where username = ?
        userService.update()
                .set("password", password)
                .eq("username", username)
                .update();
        return R.succ();
    }

    /**
     * 好友搜索
     * @return
     */
    @RequestMapping("/search")
    public R search(String keyword){
        List<User> search = userService.search(keyword);
        return R.succ(search);
    }
}

