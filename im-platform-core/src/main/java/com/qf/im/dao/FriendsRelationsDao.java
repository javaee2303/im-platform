package com.qf.im.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.im.entity.FriendsRelations;
import com.qf.im.entity.vo.FriendsVo;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 好友关系表(FriendsRelations)表数据库访问层
 *
 * @author makejava
 * @since 2023-07-28 11:00:38
 */
public interface FriendsRelationsDao extends BaseMapper<FriendsRelations> {

    @Select("""
     select u.id, if(fr.beizhu = "", u.nickname, fr.beizhu) as name, u.header as img
                from friends_relations fr
                join user u on fr.fid = u.id
                where fr.uid = #{uid}
    """)
    List<FriendsVo> friendsList(Long uid);
}

