package com.qf.im.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.im.entity.FriendsRequest;
import com.qf.im.entity.vo.FriendsRequestVo;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 好友申请表(FriendsRequest)表数据库访问层
 *
 * @author makejava
 * @since 2023-07-28 11:00:39
 */
public interface FriendsRequestDao extends BaseMapper<FriendsRequest> {

    @Select("""
        select fr.id, fr.info, fr.status, u.header, u.nickname, u.sex from `friends_request` fr 
            join user u on fr.uid = u.id where fr.tid = #{uid}
            order by fr.create_time desc
        """)
    List<FriendsRequestVo> queryRequestListByMe(Integer uid);
}

