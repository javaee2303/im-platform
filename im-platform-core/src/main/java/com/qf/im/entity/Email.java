package com.qf.im.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 邮件对象
 */
@Data
@Accessors(chain = true)
public class Email {

    private String subject;
    private String from;
    private String to;
    private String content;
}
