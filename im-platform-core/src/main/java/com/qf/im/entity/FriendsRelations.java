package com.qf.im.entity;

import com.qf.commons.domain.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 好友关系表(FriendsRelations)表实体类
 *
 * @author makejava
 * @since 2023-07-28 11:00:38
 */
@Data
@Accessors(chain = true)
public class FriendsRelations extends BaseEntity implements Serializable {
    //用户id
    private Integer uid;
    //好友id
    private Integer fid;
    //好友备注
    private String beizhu;
}

