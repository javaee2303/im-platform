package com.qf.im.entity;

import jakarta.validation.constraints.*;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class Student {

    private Integer id;
    @NotBlank(message = "姓名不能为空")
    @Size(min = 2, max = 4, message = "姓名的长度必须在{min}到{max}之间")
    private String name;
    @NotNull(message = "年龄不能为空！")
    @Min(value = 18, message = "年龄不能低于{value}")
    private Integer age;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Past(message = "生日范围不正确")
    private Date birthday;
}
