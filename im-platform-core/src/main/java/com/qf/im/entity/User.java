package com.qf.im.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.commons.domain.BaseEntity;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import java.io.Serializable;

/**
 * 用户表(User)表实体类
 *
 * @author makejava
 * @since 2023-07-26 10:14:24
 */
@Data
@Accessors(chain = true)
public class User extends BaseEntity implements Serializable {
    //主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //用户名
    @NotBlank(message = "用户名不能为空！")
    @Length(min = 6, max = 20, message = "用户名的长度必须在{min}到{max}之间")
    private String username;
    //密码
    @NotBlank(message = "密码不能为空！")
    @Length(min = 6, max = 20, message = "密码的长度必须在{min}到{max}之间")
    private String password;
    //昵称
    @NotBlank(message = "昵称不能为空！")
    private String nickname;
    //头像
    private String header;
    //性别 0-男 1-女
    @Range(min = 0, max = 1, message = "性别的格式不正确！")
    private Integer sex;
    //邮箱
    @NotBlank(message = "邮箱不能为空！")
    @Email(message = "邮箱格式不正确！")
    private String email;
}

