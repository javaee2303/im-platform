package com.qf.im.entity.vo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 好友的Vo对象
 */
@Data
@Accessors(chain = true)
public class FriendsVo {

    private Long id;
    private String name;
    private String img;
}
