package com.qf.im.service;

import com.qf.im.entity.Email;

public interface EmailService {

    void sendEmail(Email email);
}
