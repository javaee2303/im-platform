package com.qf.im.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.im.entity.FriendsRelations;
import com.qf.im.entity.vo.FriendsVo;

import java.util.List;

/**
 * 好友关系表(FriendsRelations)表服务接口
 *
 * @author makejava
 * @since 2023-07-28 11:00:38
 */
public interface FriendsRelationsService extends IService<FriendsRelations> {

    List<FriendsVo> friendsList();
}

