package com.qf.im.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.im.entity.FriendsRequest;
import com.qf.im.entity.vo.FriendsRequestVo;
import com.qf.im.entity.vo.RequestHandlerVo;

import java.util.List;

/**
 * 好友申请表(FriendsRequest)表服务接口
 *
 * @author makejava
 * @since 2023-07-28 11:00:39
 */
public interface FriendsRequestService extends IService<FriendsRequest> {

    int queryRequestCountByMe();

    List<FriendsRequestVo> queryRequestListByMe();

    int requestHandler(RequestHandlerVo requestHandlerVo);
}

