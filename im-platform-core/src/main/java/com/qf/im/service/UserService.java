package com.qf.im.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.im.entity.User;

import java.util.List;

/**
 * 用户表(User)表服务接口
 *
 * @author makejava
 * @since 2023-07-26 10:14:25
 */
public interface UserService extends IService<User> {

    User queryByUserName(String username);

    int sendEmailCode(String username);

    List<User> search(String keyword);
}

