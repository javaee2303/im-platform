package com.qf.im.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.commons.aspect.annotation.GetUser;
import com.qf.commons.utils.AuthUtils;
import com.qf.im.dao.FriendsRelationsDao;
import com.qf.im.entity.FriendsRelations;
import com.qf.im.entity.vo.FriendsVo;
import com.qf.im.service.FriendsRelationsService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 好友关系表(FriendsRelations)表服务实现类
 *
 * @author makejava
 * @since 2023-07-28 11:00:39
 */
@Service("friendsRelationsService")
public class FriendsRelationsServiceImpl extends ServiceImpl<FriendsRelationsDao, FriendsRelations> implements FriendsRelationsService {

    @Override
    @GetUser
    public List<FriendsVo> friendsList() {
        //当前用户id
        Integer uid = AuthUtils.get();
        return getBaseMapper().friendsList(uid.longValue());
    }
}

