package com.qf.im.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.commons.aspect.annotation.GetUser;
import com.qf.commons.domain.Codes;
import com.qf.commons.exception.ServiceException;
import com.qf.commons.utils.AuthUtils;
import com.qf.im.dao.FriendsRequestDao;
import com.qf.im.entity.FriendsRelations;
import com.qf.im.entity.FriendsRequest;
import com.qf.im.entity.vo.FriendsRequestVo;
import com.qf.im.entity.vo.RequestHandlerVo;
import com.qf.im.service.FriendsRelationsService;
import com.qf.im.service.FriendsRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 好友申请表(FriendsRequest)表服务实现类
 *
 * @author makejava
 * @since 2023-07-28 11:00:40
 */
@Service("friendsRequestService")
public class FriendsRequestServiceImpl extends ServiceImpl<FriendsRequestDao, FriendsRequest> implements FriendsRequestService {

    @Autowired
    private FriendsRelationsService friendsRelationsService;

    /**
     * 新增好友申请信息
     * @param entity
     * @return
     */
    @Override
    @GetUser
    public boolean save(FriendsRequest entity) {
        //获得当前的申请者id
        Integer uid = AuthUtils.get();
        entity.setUid(uid);

        if (uid.equals(entity.getTid())) {
            //不能申请自己为好友
            throw new ServiceException(Codes.NOT_REQUEST_SELF);
        }

        //查询是否已经存在申请对象
        FriendsRequest request = this.query().eq("uid", entity.getUid())
                .eq("tid", entity.getTid())
                .one();

        if (request != null) {
            //已经申请过
            if (request.getStatus() == 0) {
                throw new ServiceException(Codes.REQUEST_EXISTS);
            }

            if (request.getStatus() == 1) {
                throw new ServiceException(Codes.FRIENDS_EXISTS);
            }
        }

        return super.save(entity);
    }

    @Override
    @GetUser
    public int queryRequestCountByMe() {
        //1、获得我的用户id
        Integer uid = AuthUtils.get();
        //2、查询数据库
        Long count = this.query()
                .eq("tid", uid)
                .eq("status", 0)
                .count();
        return count.intValue();
    }

    @Override
    @GetUser
    public List<FriendsRequestVo> queryRequestListByMe() {
        Integer uid = AuthUtils.get();
        List<FriendsRequestVo> friendsRequestVos = getBaseMapper().queryRequestListByMe(uid);
        return friendsRequestVos;
    }

    @Override
    @Transactional
    public int requestHandler(RequestHandlerVo requestHandlerVo) {

        //修改当前申请记录的状态
        this.update()
                .set("status", requestHandlerVo.getStatus())
                .eq("id", requestHandlerVo.getId())
                .update();

        if (requestHandlerVo.getStatus() == 2) {
            //拒绝当前的好友申请
            return 1;
        }

        if (requestHandlerVo.getStatus() == 1) {
            //同意当前的好友申请
            //1、查询出当前的申请记录
            FriendsRequest request = this.getById(requestHandlerVo.getId());

            //2、校验 双方是否已经是好友了
            Long count = friendsRelationsService.query()
                    .eq("uid", request.getUid())
                    .eq("fid", request.getTid())
                    .count();

            if (count > 0) {
                throw new ServiceException(Codes.FRIENDS_EXISTS);
            }

            //3、添加好友关系
            FriendsRelations friendsRelations = new FriendsRelations()
                    .setUid(request.getUid())
                    .setFid(request.getTid())
                    .setBeizhu(request.getBeizhu());

            FriendsRelations friendsRelations2 = new FriendsRelations()
                    .setUid(request.getTid())
                    .setFid(request.getUid())
                    .setBeizhu(requestHandlerVo.getBeizhu());

            friendsRelationsService.save(friendsRelations);
            friendsRelationsService.save(friendsRelations2);
        }

        return 1;
    }
}

