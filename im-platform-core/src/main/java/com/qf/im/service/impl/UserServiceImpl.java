package com.qf.im.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.commons.domain.Codes;
import com.qf.commons.exception.ServiceException;
import com.qf.commons.utils.CodeUtils;
import com.qf.im.dao.UserDao;
import com.qf.im.entity.Email;
import com.qf.im.entity.User;
import com.qf.im.service.EmailService;
import com.qf.im.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 用户表(User)表服务实现类
 *
 * @author makejava
 * @since 2023-07-26 10:14:25
 */
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {

    @Autowired
    private EmailService emailService;

    @Override
    public boolean save(User entity) {
        //校验用户名是否存在
        Long userCount = this.query().eq("username", entity.getUsername()).count();
        if (userCount != 0) {
            //用户名已经存在
            throw new ServiceException(Codes.USER_EXISTS);
        }

        //校验邮箱是否存在
        Long emailCount = this.query().eq("email", entity.getEmail()).count();
        if (emailCount != 0) {
            //邮箱已经存在
            throw new ServiceException(Codes.EMAIL_EXISTS);
        }

        return super.save(entity);
    }

    @Override
    public User queryByUserName(String username) {
        return this.query().eq("username", username).one();
    }

    @Override
    public int sendEmailCode(String username) {
        //1、更具用户名查找用户 - 邮箱地址
        User user = this.queryByUserName(username);
        if (user == null) {
            throw new ServiceException(Codes.USER_NOT_FOUND);
        }

        //获取需要接收邮件的邮箱
        String sendEmail = user.getEmail();

        //2、生成一个验证码，发送到指定的邮箱
        int sendcode = (int)(Math.random() * 9000 + 1000);

        //3、缓存这个验证码，缓存到哪里？（数据库、session、cookie、redis！！、ThreadLocal、Map（username - code））
        CodeUtils.putCode(username, sendcode, 5, TimeUnit.MINUTES);

        Email email = new Email()
                .setSubject("【腾讯BB官方邮件】找回密码")
                .setTo(sendEmail)
                .setContent("找回密码的验证码内容：" + sendcode + ", 如果不是本人操作，请忽略！");
        //发送邮件
        emailService.sendEmail(email);

        return 1;
    }

    @Override
    public List<User> search(String keyword) {
        if (!StringUtils.hasLength(keyword)) {
            //关键字为空
            return null;
        }

        List<User> users = this.query()
                .select("id", "nickname", "header", "sex")
                .like("username", keyword)
                .or()
                .like("nickname", keyword)
                .list();
//                .stream().map(user -> {
//                    user.setPassword(null);
//                    return user;
//                }).collect(Collectors.toList());

        return users;
    }
}

