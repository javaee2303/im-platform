package com.qf.im.websocket;

import com.qf.im.websocket.base.BaseChannelInHandler;
import com.qf.im.websocket.base.BaseChannelOutHandler;
import com.qf.im.websocket.handler.TextMsgInChannelHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * WebSocket服务器的启动类
 */
@Component
@Slf4j
public class WsServer {

    private EventLoopGroup master = new NioEventLoopGroup();
    private EventLoopGroup slave = new NioEventLoopGroup();

    @Value("${server.ws.port}")
    private int port;

    /**
     * 将IOC容器中所有BaseChannelInHandler类型的Bean，全部注入到这个List集合里面
     */
    @Autowired(required = false)
    private List<BaseChannelInHandler> baseChannelInHandlers;

    /**
     * 启动WebSocket服务
     */
    public void start(){
        ServerBootstrap serverBootstrap = new ServerBootstrap()
                .group(master, slave)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel nioSocketChannel) throws Exception {
                        //channel流水线
                        ChannelPipeline pipeline = nioSocketChannel.pipeline();
                        //【责任链模式】
                        //Http协议的处理
                        pipeline.addLast(new HttpServerCodec());
                        pipeline.addLast(new HttpObjectAggregator(1024 * 1024));
                        //WebSocket协议的处理
                        pipeline.addLast(new WebSocketServerProtocolHandler("/im"));
                        //自定义消息的处理
                        pipeline.addLast(new TextMsgInChannelHandler());
                        //出栈消息处理器
                        pipeline.addLast(new BaseChannelOutHandler());
                        //将开发者自定义的ChannelInHandler添加到流水线中
                        Optional.ofNullable(baseChannelInHandlers)
                                .ifPresent(baseChannelInHandlers1 -> {
                                    for (BaseChannelInHandler baseChannelInHandler : baseChannelInHandlers1) {
                                        //不同Channel的流水线，添加了同一个ChannelHandler对象
                                        //被共享的ChannelHandler一定要标记@Sharable注解
                                        pipeline.addLast(baseChannelInHandler);
                                    }
                                });
                    }
                });

        ChannelFuture channelFuture = serverBootstrap.bind(port);
        channelFuture.addListener(future -> {
            if (future.isSuccess()) {
                log.info("【websocket start】websocket服务启动成功... port:{}", port);
            } else {
                log.error("【websocket start】websocket服务启动失败!!!", future.cause());
                //强行停止SpringBoot
                System.exit(1);
            }
        });
    }

    /**
     * 销毁WebSocket服务
     */
    public void destory(){
        log.info("【websocket stop】websocket服务停止运行....");
        //Netty优雅关机，Netty会拒绝新的连接和新的消息，但是会将还未处理完的消息处理完
        master.shutdownGracefully();
        slave.shutdownGracefully();
    }
}
