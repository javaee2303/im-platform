package com.qf.im.websocket.base;

import com.alibaba.fastjson.JSON;
import com.qf.im.entity.Message;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

/**
 * 统一处理Message消息的出栈
 */
public class BaseChannelOutHandler extends ChannelOutboundHandlerAdapter {

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        if (msg instanceof Message m) {
            String json = JSON.toJSONString(m);
            msg = new TextWebSocketFrame(json);
        }

        super.write(ctx, msg, promise);
    }
}
