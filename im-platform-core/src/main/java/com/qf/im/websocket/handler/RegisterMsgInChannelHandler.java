package com.qf.im.websocket.handler;

import com.qf.im.entity.Message;
import com.qf.im.websocket.base.BaseChannelInHandler;
import com.qf.im.websocket.utils.ChannelManager;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import org.springframework.stereotype.Component;

/**
 * 处理注册消息
 */
@Component
@ChannelHandler.Sharable
public class RegisterMsgInChannelHandler extends BaseChannelInHandler {

    @Override
    protected int[] action() {
        return new int[]{100};
    }

    @Override
    protected void handler(ChannelHandlerContext ctx, Message message) {
        //按照注册消息去处理
        Integer uid = message.getFrom();
        Channel channel = ctx.channel();
        //注册消息
        ChannelManager.put(uid, channel);
    }
}
