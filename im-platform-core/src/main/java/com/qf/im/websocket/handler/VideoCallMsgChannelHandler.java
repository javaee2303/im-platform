package com.qf.im.websocket.handler;

import com.qf.im.entity.Message;
import com.qf.im.websocket.base.BaseChannelInHandler;
import com.qf.im.websocket.utils.ChannelManager;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import org.springframework.stereotype.Component;

@Component
@ChannelHandler.Sharable
public class VideoCallMsgChannelHandler extends BaseChannelInHandler {
    @Override
    protected int[] action() {
        return new int[]{201,202,203,204};
    }

    @Override
    protected void handler(ChannelHandlerContext ctx, Message message) {
        Channel channel = ChannelManager.getVideoChannel(message.getTo());
        if (channel != null) {
            channel.writeAndFlush(message);
        }
    }
}
