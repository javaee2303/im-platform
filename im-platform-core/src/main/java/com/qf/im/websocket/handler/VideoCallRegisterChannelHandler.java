package com.qf.im.websocket.handler;

import com.qf.im.entity.Message;
import com.qf.im.websocket.base.BaseChannelInHandler;
import com.qf.im.websocket.utils.ChannelManager;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import org.springframework.stereotype.Component;

@Component
@ChannelHandler.Sharable
public class VideoCallRegisterChannelHandler extends BaseChannelInHandler {
    @Override
    protected int[] action() {
        return new int[]{200};
    }

    @Override
    protected void handler(ChannelHandlerContext ctx, Message message) {
        Integer uid = message.getFrom();
        ChannelManager.putVideo(uid, ctx.channel());
    }
}
