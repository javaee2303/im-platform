package com.qf.im.websocket.utils;

import io.netty.channel.Channel;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class ChannelManager {

    private static Map<Integer, Channel> channelMap = new ConcurrentHashMap<>();
    private static Map<Channel, Integer> channelMap2 = new ConcurrentHashMap<>();

    private static Map<Integer, Channel> videochannelMap = new ConcurrentHashMap<>();
    private static Map<Channel, Integer> videochannelMap2 = new ConcurrentHashMap<>();

    public static void put(Integer uid, Channel channel) {
        channelMap.put(uid, channel);
        channelMap2.put(channel, uid);
    }

    public static Channel get(Integer uid) {
        return channelMap.get(uid);
    }


    public static void remove(Integer uid){
        Channel channel = channelMap.remove(uid);
        if (channel != null) {
            channelMap2.remove(channel);
        }
    }

    public static void remove(Channel channel){
        Integer uid = channelMap2.remove(channel);
        Optional.ofNullable(uid).ifPresent(u -> {
            channelMap.remove(u);
        });

    }

    public static void putVideo(Integer uid, Channel channel){
        videochannelMap.put(uid, channel);
        videochannelMap2.put(channel, uid);
    }

    public static Channel getVideoChannel(Integer uid) {
        return videochannelMap.get(uid);
    }

    public static void removeVideoChannel(Integer uid){
        Channel channel = videochannelMap.remove(uid);
        Optional.ofNullable(channel).ifPresent(c -> {
            videochannelMap2.remove(c);
        });
    }

    public static void removeVideoChannel(Channel channel){
        Integer uid = videochannelMap2.remove(channel);
        Optional.ofNullable(uid).ifPresent(integer -> {
            videochannelMap.remove(integer);
        });

    }


}
