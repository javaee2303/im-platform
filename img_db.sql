create database im_db;

#用户表
create table user(
id int primary key auto_increment comment '主键',
username varchar(20) unique not null comment '用户名',
password varchar(20) not null comment '密码',
nickname varchar(10) not null comment '昵称',
header text comment '头像',
sex tinyint comment '性别 0-男 1-女',
email varchar(30) not null unique comment '邮箱',
create_time datetime not null comment '创建时间',
update_time datetime not null comment '最后修改时间',
status tinyint not null comment '状态',
del_flag tinyint not null comment '删除标识 0-正常 1-已经删除'
) comment '用户表';

#好友申请表
create table friends_request(
id int primary key auto_increment comment '主键',
uid int not null comment '申请者id',
tid int not null comment '被申请者id',
beizhu varchar(10) comment '好友备注',
info varchar(50) comment '申请信息',
create_time datetime not null comment '创建时间',
update_time datetime not null comment '最后修改时间',
status tinyint not null comment '状态(0-待处理 1-同意 2-拒绝)',
del_flag tinyint not null comment '删除标识 0-正常 1-已经删除'
) comment "好友申请表";

#好友关系表
create table friends_relations(
  uid int not null comment '用户id',
  fid int not null comment '好友id',
  beizhu varchar(10) comment '好友备注',
  create_time datetime not null comment '创建时间',
  update_time datetime not null comment '最后修改时间',
  status tinyint not null comment '状态(0-正常好友 1-黑名单)',
  del_flag tinyint not null comment '删除标识 0-正常 1-已经删除',
  primary key(uid, fid) #联合主键
) comment '好友关系表';